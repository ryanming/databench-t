/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.controller;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.common.constant.TrancfgConstant;
import com.common.context.SpringContextUtils;
import com.pojo.DataConfig;
import com.pojo.Trancfg;
import com.service.ConfigureService;
import com.service.impl.QueryAccountServiceImpl;

@RestController
public class QueryAccountController {

    private final Logger logger = LoggerFactory.getLogger(QueryAccountController.class); 

    @Autowired
    private ConfigureService configureService;
    
    @RequestMapping("/queryAccount")
    public String queryAccount(){
    	String datacnfg_id = "1";//数据编号
    	String trancfg_testid = "1";//测试编号
    	DataConfig df = configureService.readDataConfig(datacnfg_id);
    	Trancfg tf = configureService.readTranConfig(trancfg_testid,TrancfgConstant.TRANCFG_ACCOUNTS_QUERY);
       	Integer procnum = tf.getTrancfg_procnum();//实例数
       	Integer runnum = tf.getTrancfg_runnum();//运行次数/实例
        ExecutorService executor = Executors.newFixedThreadPool(procnum);
        for (int i = 0; i < procnum; i++) {
        	QueryAccountServiceImpl queryAccountServiceImpl = SpringContextUtils.getBean(QueryAccountServiceImpl.class);
        	queryAccountServiceImpl.setDatacfg_accnum(df.getDatacfg_accnum());
        	queryAccountServiceImpl.setNum(runnum/procnum);
            executor.submit(queryAccountServiceImpl);
		}
        //关闭线程池
        executor.shutdown();
        try {
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            logger.error(" QueryAccountController queryAccount thread InterruptedException {} ", e);
        }
        return "账户查询测试结束";
    }
}
