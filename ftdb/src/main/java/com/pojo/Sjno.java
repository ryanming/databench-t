/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.pojo;

import java.math.BigDecimal;

public class Sjno {

    private String sjno_id; //科目号
    private String sjno_branchid; //网点号
    private BigDecimal  sjno_bale; //科目余额
    private String sjno_name; //科目名称
    private String sjno_fld1; 
    private String sjno_fld2;
    private BigDecimal  sjno_fld3; 
    private BigDecimal  sjno_fld4;
    public String getSjno_id() {
        return sjno_id;
    }

    public void setSjno_id(String sjno_id) {
        this.sjno_id = sjno_id;
    }

    public String getSjno_branchid() {
        return sjno_branchid;
    }

    public void setSjno_branchid(String sjno_branchid) {
        this.sjno_branchid = sjno_branchid;
    }

    public BigDecimal getSjno_bale() {
        return sjno_bale;
    }

    public void setSjno_bale(BigDecimal sjno_bale) {
        this.sjno_bale = sjno_bale;
    }

    public String getSjno_name() {
        return sjno_name;
    }

    public void setSjno_name(String sjno_name) {
        this.sjno_name = sjno_name;
    }

	public String getSjno_fld1() {
		return sjno_fld1;
	}

	public void setSjno_fld1(String sjno_fld1) {
		this.sjno_fld1 = sjno_fld1;
	}

	public String getSjno_fld2() {
		return sjno_fld2;
	}

	public void setSjno_fld2(String sjno_fld2) {
		this.sjno_fld2 = sjno_fld2;
	}

	public BigDecimal getSjno_fld3() {
		return sjno_fld3;
	}

	public void setSjno_fld3(BigDecimal sjno_fld3) {
		this.sjno_fld3 = sjno_fld3;
	}

	public BigDecimal getSjno_fld4() {
		return sjno_fld4;
	}

	public void setSjno_fld4(BigDecimal sjno_fld4) {
		this.sjno_fld4 = sjno_fld4;
	}
    
    
}
