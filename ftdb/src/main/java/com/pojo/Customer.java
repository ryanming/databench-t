/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.pojo;

import java.math.BigDecimal;
import java.sql.Time;
import java.util.Date;

public class Customer {

    private String customer_id; //客户号
    private String customer_idtype; //证件类型
    private String customer_idno; //证件号码
    private String customer_name; //客户姓名
    private String customer_add; //地址
    private Date   customer_birthday; //出生日期
    private String customer_tel; //电话
    private Date   customer_cdate; //客户创建日期
    private String customer_cbrhid; //客户创建网点
    private BigDecimal  customer_bale; //账户资产余额
    private Date   customer_mdate; //最后修改日期
    private Time   customer_mtime; //最后修改时间

    private String customer_fld1; 
    private String customer_fld2;
    private BigDecimal  customer_fld3; 
    private BigDecimal  customer_fld4;
    
    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getCustomer_idtype() {
        return customer_idtype;
    }

    public void setCustomer_idtype(String customer_idtype) {
        this.customer_idtype = customer_idtype;
    }

    public String getCustomer_idno() {
        return customer_idno;
    }

    public void setCustomer_idno(String customer_idno) {
        this.customer_idno = customer_idno;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getCustomer_add() {
        return customer_add;
    }

    public void setCustomer_add(String customer_add) {
        this.customer_add = customer_add;
    }

    public Date getCustomer_birthday() {
        return customer_birthday;
    }

    public void setCustomer_birthday(Date customer_birthday) {
        this.customer_birthday = customer_birthday;
    }

    public String getCustomer_tel() {
        return customer_tel;
    }

    public void setCustomer_tel(String customer_tel) {
        this.customer_tel = customer_tel;
    }

    public Date getCustomer_cdate() {
        return customer_cdate;
    }

    public void setCustomer_cdate(Date customer_cdate) {
        this.customer_cdate = customer_cdate;
    }

    public String getCustomer_cbrhid() {
        return customer_cbrhid;
    }

    public void setCustomer_cbrhid(String customer_cbrhid) {
        this.customer_cbrhid = customer_cbrhid;
    }

    public BigDecimal getCustomer_bale() {
        return customer_bale;
    }

    public void setCustomer_bale(BigDecimal customer_bale) {
        this.customer_bale = customer_bale;
    }

    public Date getCustomer_mdate() {
        return customer_mdate;
    }

    public void setCustomer_mdate(Date customer_mdate) {
        this.customer_mdate = customer_mdate;
    }

    public Time getCustomer_mtime() {
        return customer_mtime;
    }

    public void setCustomer_mtime(Time customer_mtime) {
        this.customer_mtime = customer_mtime;
    }

	public String getCustomer_fld1() {
		return customer_fld1;
	}

	public void setCustomer_fld1(String customer_fld1) {
		this.customer_fld1 = customer_fld1;
	}

	public String getCustomer_fld2() {
		return customer_fld2;
	}

	public void setCustomer_fld2(String customer_fld2) {
		this.customer_fld2 = customer_fld2;
	}

	public BigDecimal getCustomer_fld3() {
		return customer_fld3;
	}

	public void setCustomer_fld3(BigDecimal customer_fld3) {
		this.customer_fld3 = customer_fld3;
	}

	public BigDecimal getCustomer_fld4() {
		return customer_fld4;
	}

	public void setCustomer_fld4(BigDecimal customer_fld4) {
		this.customer_fld4 = customer_fld4;
	}
    
    
}
