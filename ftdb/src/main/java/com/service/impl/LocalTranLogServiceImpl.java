/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.localMapper.LocalTranLogMapper;
import com.pojo.Tranlog;
import com.service.LocalTranLogService;

@Service
@Scope("prototype")
public class LocalTranLogServiceImpl extends Thread implements LocalTranLogService{
    @Autowired
    private LocalTranLogMapper localTranLogMapper;
    
    private Tranlog tranlog;
    
    private final Logger logger = LoggerFactory.getLogger(LocalTranLogServiceImpl.class); 
    @Override
	public void run() {
    	try {
    		localTranLogMapper.insertTranlog(tranlog);
    		logger.info("LocalTranLogServiceImpl run tranlog_date {} tranlog_branchid {} tranlog_branchseq {} success "
    				,tranlog.getTranlog_date(),tranlog.getTranlog_branchid(),tranlog.getTranlog_branchseq());
		} catch (Exception e) {
			logger.error("LocalTranLogServiceImpl run rollback tranlog_date {} tranlog_branchid {} tranlog_branchseq {} e {}" 
					,tranlog.getTranlog_date(),tranlog.getTranlog_branchid(),tranlog.getTranlog_branchseq(),e);
		}
	}
	public Tranlog getTranlog() {
		return tranlog;
	}
	public void setTranlog(Tranlog tranlog) {
		this.tranlog = tranlog;
	}

    
}